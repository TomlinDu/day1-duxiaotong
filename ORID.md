Daily Report (2023/07/10) 
1.O(Objective): What did we learn today? What activities did you do? What scenes have impressed you? 
o	This morning, we played the ice-breaking game "Two true and one false". Then, we learned the PDCA process to improve efficiency, and shared expectations and some learning skills with others.  
o	In the afternoon, we learned concept map and drew concept map in groups. I also learned about Standup meeting and what is ORID.  
o	What impressed me deeply was the picture of the teacher leaning against the door to take a group photo of us. 
2.R(Reflective): Please use one word to express your feelings about today’s class. 
o	Happy. 
3.I(Interpretive): What do you think about this? What was the most meaningful aspect of this activity? 
o	I am very happy today, because the learning method is relatively easy, and in the learning process, I have more communication with others and gradually get familiar with them. 
4.D(Decisional): Where do you most want to apply what you have learned today? What changes will you make? 
o	I will apply what I learned today to my future work, summarize in a structured way, and use PDCA to improve my efficiency 
